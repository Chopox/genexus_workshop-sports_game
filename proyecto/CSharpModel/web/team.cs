/*
               File: Team
        Description: Team
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/15/2022 4:14:32.52
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class team : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A1DisciplineId = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1DisciplineId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1DisciplineId), 4, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A1DisciplineId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A6AthleteId = (short)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A6AthleteId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_5") == 0 )
         {
            A3CountryId = (short)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_5( A3CountryId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridteam_athlete") == 0 )
         {
            nRC_GXsfl_68 = (int)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_68_idx = (int)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_68_idx = GetNextPar( );
            Gx_mode = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridteam_athlete_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_5-135614", 0) ;
            Form.Meta.addItem("description", "Team", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtTeamid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public team( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public team( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            DrawControls( ) ;
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void DrawControls( )
      {
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Team", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         ClassString = "ErrorViewer";
         StyleString = "";
         GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
         ClassString = "BtnFirst";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
         ClassString = "BtnPrevious";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
         ClassString = "BtnNext";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
         ClassString = "BtnLast";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
         ClassString = "BtnSelect";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 4, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx0050.aspx"+"',["+"{Ctrl:gx.dom.el('"+"TEAMID"+"'), id:'"+"TEAMID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtTeamid_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtTeamid_Internalname, "Teamid", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtTeamid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A14Teamid), 4, 0, ".", "")), ((edtTeamid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A14Teamid), "ZZZ9")) : context.localUtil.Format( (decimal)(A14Teamid), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTeamid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtTeamid_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtTeamCountryId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtTeamCountryId_Internalname, "Country Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtTeamCountryId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A16TeamCountryId), 4, 0, ".", "")), ((edtTeamCountryId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A16TeamCountryId), "ZZZ9")) : context.localUtil.Format( (decimal)(A16TeamCountryId), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTeamCountryId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtTeamCountryId_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtTeamCountryName_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtTeamCountryName_Internalname, "Country Name", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtTeamCountryName_Internalname, StringUtil.RTrim( A17TeamCountryName), StringUtil.RTrim( context.localUtil.Format( A17TeamCountryName, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTeamCountryName_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtTeamCountryName_Enabled, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+imgTeamContryFlag_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, "", "Contry Flag", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Static Bitmap Variable */
         ClassString = "Attribute";
         StyleString = "";
         A30TeamContryFlag_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( A30TeamContryFlag))&&String.IsNullOrEmpty(StringUtil.RTrim( A40000TeamContryFlag_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( A30TeamContryFlag)));
         sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( A30TeamContryFlag)) ? A40000TeamContryFlag_GXI : context.PathToRelativeUrl( A30TeamContryFlag));
         GxWebStd.gx_bitmap( context, imgTeamContryFlag_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, imgTeamContryFlag_Enabled, "", "", 1, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 0, A30TeamContryFlag_IsBlob, true, context.GetImageSrcSet( sImgUrl), "HLP_Team.htm");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTeamContryFlag_Internalname, "URL", (String.IsNullOrEmpty(StringUtil.RTrim( A30TeamContryFlag)) ? A40000TeamContryFlag_GXI : context.PathToRelativeUrl( A30TeamContryFlag)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTeamContryFlag_Internalname, "IsBlob", StringUtil.BoolToStr( A30TeamContryFlag_IsBlob), true);
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtDisciplineId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtDisciplineId_Internalname, "Discipline Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtDisciplineId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1DisciplineId), 4, 0, ".", "")), ((edtDisciplineId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1DisciplineId), "ZZZ9")) : context.localUtil.Format( (decimal)(A1DisciplineId), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtDisciplineId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtDisciplineId_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Team.htm");
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         GxWebStd.gx_bitmap( context, imgprompt_1_Internalname, sImgUrl, imgprompt_1_Link, "", "", context.GetTheme( ), imgprompt_1_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtDisciplineName_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtDisciplineName_Internalname, "Discipline Name", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtDisciplineName_Internalname, StringUtil.RTrim( A2DisciplineName), StringUtil.RTrim( context.localUtil.Format( A2DisciplineName, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtDisciplineName_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtDisciplineName_Enabled, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 LevelTable", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divAthletetable_Internalname, 1, 0, "px", 0, "px", "LevelTable", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitleathlete_Internalname, "Athlete", "", "", lblTitleathlete_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         gxdraw_Gridteam_athlete( ) ;
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
         ClassString = "BtnEnter";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
         ClassString = "BtnCancel";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
         ClassString = "BtnDelete";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Team.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "Center", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
      }

      protected void gxdraw_Gridteam_athlete( )
      {
         /*  Grid Control  */
         Gridteam_athleteContainer.AddObjectProperty("GridName", "Gridteam_athlete");
         Gridteam_athleteContainer.AddObjectProperty("Header", subGridteam_athlete_Header);
         Gridteam_athleteContainer.AddObjectProperty("Class", "Grid");
         Gridteam_athleteContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Gridteam_athleteContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Gridteam_athleteContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridteam_athlete_Backcolorstyle), 1, 0, ".", "")));
         Gridteam_athleteContainer.AddObjectProperty("CmpContext", "");
         Gridteam_athleteContainer.AddObjectProperty("InMasterPage", "false");
         Gridteam_athleteColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridteam_athleteColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A6AthleteId), 4, 0, ".", "")));
         Gridteam_athleteColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAthleteId_Enabled), 5, 0, ".", "")));
         Gridteam_athleteContainer.AddColumnProperties(Gridteam_athleteColumn);
         Gridteam_athleteColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridteam_athleteContainer.AddColumnProperties(Gridteam_athleteColumn);
         Gridteam_athleteColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridteam_athleteColumn.AddObjectProperty("Value", StringUtil.RTrim( A29Athlete_Name));
         Gridteam_athleteColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAthlete_Name_Enabled), 5, 0, ".", "")));
         Gridteam_athleteContainer.AddColumnProperties(Gridteam_athleteColumn);
         Gridteam_athleteColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridteam_athleteColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3CountryId), 4, 0, ".", "")));
         Gridteam_athleteColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCountryId_Enabled), 5, 0, ".", "")));
         Gridteam_athleteContainer.AddColumnProperties(Gridteam_athleteColumn);
         Gridteam_athleteColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridteam_athleteColumn.AddObjectProperty("Value", StringUtil.RTrim( A4CountryName));
         Gridteam_athleteColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCountryName_Enabled), 5, 0, ".", "")));
         Gridteam_athleteContainer.AddColumnProperties(Gridteam_athleteColumn);
         Gridteam_athleteContainer.AddObjectProperty("Selectedindex", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridteam_athlete_Selectedindex), 4, 0, ".", "")));
         Gridteam_athleteContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridteam_athlete_Allowselection), 1, 0, ".", "")));
         Gridteam_athleteContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridteam_athlete_Selectioncolor), 9, 0, ".", "")));
         Gridteam_athleteContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridteam_athlete_Allowhovering), 1, 0, ".", "")));
         Gridteam_athleteContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridteam_athlete_Hoveringcolor), 9, 0, ".", "")));
         Gridteam_athleteContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridteam_athlete_Allowcollapsing), 1, 0, ".", "")));
         Gridteam_athleteContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridteam_athlete_Collapsed), 1, 0, ".", "")));
         nGXsfl_68_idx = 0;
         if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
         {
            /* Enter key processing. */
            nBlankRcdCount8 = 5;
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               /* Display confirmed (stored) records */
               nRcdExists_8 = 1;
               ScanStart058( ) ;
               while ( RcdFound8 != 0 )
               {
                  init_level_properties8( ) ;
                  getByPrimaryKey058( ) ;
                  AddRow058( ) ;
                  ScanNext058( ) ;
               }
               ScanEnd058( ) ;
               nBlankRcdCount8 = 5;
            }
         }
         else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
         {
            /* Button check  or addlines. */
            standaloneNotModal058( ) ;
            standaloneModal058( ) ;
            sMode8 = Gx_mode;
            while ( nGXsfl_68_idx < nRC_GXsfl_68 )
            {
               bGXsfl_68_Refreshing = true;
               ReadRow058( ) ;
               edtAthleteId_Enabled = (int)(context.localUtil.CToN( cgiGet( "ATHLETEID_"+sGXsfl_68_idx+"Enabled"), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAthleteId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAthleteId_Enabled), 5, 0)), !bGXsfl_68_Refreshing);
               edtAthlete_Name_Enabled = (int)(context.localUtil.CToN( cgiGet( "ATHLETE_NAME_"+sGXsfl_68_idx+"Enabled"), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAthlete_Name_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAthlete_Name_Enabled), 5, 0)), !bGXsfl_68_Refreshing);
               edtCountryId_Enabled = (int)(context.localUtil.CToN( cgiGet( "COUNTRYID_"+sGXsfl_68_idx+"Enabled"), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCountryId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCountryId_Enabled), 5, 0)), !bGXsfl_68_Refreshing);
               edtCountryName_Enabled = (int)(context.localUtil.CToN( cgiGet( "COUNTRYNAME_"+sGXsfl_68_idx+"Enabled"), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCountryName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCountryName_Enabled), 5, 0)), !bGXsfl_68_Refreshing);
               imgprompt_1_Link = cgiGet( "PROMPT_6_"+sGXsfl_68_idx+"Link");
               if ( ( nRcdExists_8 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal058( ) ;
               }
               SendRow058( ) ;
               bGXsfl_68_Refreshing = false;
            }
            Gx_mode = sMode8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            /* Get or get-alike key processing. */
            nBlankRcdCount8 = 5;
            nRcdExists_8 = 1;
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               ScanStart058( ) ;
               while ( RcdFound8 != 0 )
               {
                  sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx+1), 4, 0)), 4, "0");
                  SubsflControlProps_688( ) ;
                  init_level_properties8( ) ;
                  standaloneNotModal058( ) ;
                  getByPrimaryKey058( ) ;
                  standaloneModal058( ) ;
                  AddRow058( ) ;
                  ScanNext058( ) ;
               }
               ScanEnd058( ) ;
            }
         }
         /* Initialize fields for 'new' records and send them. */
         sMode8 = Gx_mode;
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx+1), 4, 0)), 4, "0");
         SubsflControlProps_688( ) ;
         InitAll058( ) ;
         init_level_properties8( ) ;
         nRcdExists_8 = 0;
         nIsMod_8 = 0;
         nRcdDeleted_8 = 0;
         nBlankRcdCount8 = (short)(nBlankRcdUsr8+nBlankRcdCount8);
         fRowAdded = 0;
         while ( nBlankRcdCount8 > 0 )
         {
            standaloneNotModal058( ) ;
            standaloneModal058( ) ;
            AddRow058( ) ;
            if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
            {
               fRowAdded = 1;
               GX_FocusControl = edtAthleteId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nBlankRcdCount8 = (short)(nBlankRcdCount8-1);
         }
         Gx_mode = sMode8;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         sStyleString = "";
         context.WriteHtmlText( "<div id=\""+"Gridteam_athleteContainer"+"Div\" "+sStyleString+">"+"</div>") ;
         context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridteam_athlete", Gridteam_athleteContainer);
         if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridteam_athleteContainerData", Gridteam_athleteContainer.ToJavascriptSource());
         }
         if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridteam_athleteContainerData"+"V", Gridteam_athleteContainer.GridValuesHidden());
         }
         else
         {
            context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Gridteam_athleteContainerData"+"V"+"\" value='"+Gridteam_athleteContainer.GridValuesHidden()+"'/>") ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtTeamid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTeamid_Internalname), ".", ",") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TEAMID");
               AnyError = 1;
               GX_FocusControl = edtTeamid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A14Teamid = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Teamid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14Teamid), 4, 0)));
            }
            else
            {
               A14Teamid = (short)(context.localUtil.CToN( cgiGet( edtTeamid_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Teamid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14Teamid), 4, 0)));
            }
            A16TeamCountryId = (short)(context.localUtil.CToN( cgiGet( edtTeamCountryId_Internalname), ".", ","));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A16TeamCountryId", StringUtil.LTrim( StringUtil.Str( (decimal)(A16TeamCountryId), 4, 0)));
            A17TeamCountryName = cgiGet( edtTeamCountryName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17TeamCountryName", A17TeamCountryName);
            A30TeamContryFlag = cgiGet( imgTeamContryFlag_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30TeamContryFlag", A30TeamContryFlag);
            if ( ( ( context.localUtil.CToN( cgiGet( edtDisciplineId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtDisciplineId_Internalname), ".", ",") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "DISCIPLINEID");
               AnyError = 1;
               GX_FocusControl = edtDisciplineId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A1DisciplineId = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1DisciplineId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1DisciplineId), 4, 0)));
            }
            else
            {
               A1DisciplineId = (short)(context.localUtil.CToN( cgiGet( edtDisciplineId_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1DisciplineId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1DisciplineId), 4, 0)));
            }
            A2DisciplineName = cgiGet( edtDisciplineName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2DisciplineName", A2DisciplineName);
            /* Read saved values. */
            Z14Teamid = (short)(context.localUtil.CToN( cgiGet( "Z14Teamid"), ".", ","));
            Z1DisciplineId = (short)(context.localUtil.CToN( cgiGet( "Z1DisciplineId"), ".", ","));
            IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
            IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
            Gx_mode = cgiGet( "Mode");
            nRC_GXsfl_68 = (int)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_68"), ".", ","));
            A40000TeamContryFlag_GXI = cgiGet( "TEAMCONTRYFLAG_GXI");
            n40000TeamContryFlag_GXI = (String.IsNullOrEmpty(StringUtil.RTrim( A40000TeamContryFlag_GXI))&&String.IsNullOrEmpty(StringUtil.RTrim( A30TeamContryFlag)) ? true : false);
            Gx_mode = cgiGet( "vMODE");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            getMultimediaValue(imgTeamContryFlag_Internalname, ref  A30TeamContryFlag, ref  A40000TeamContryFlag_GXI);
            n40000TeamContryFlag_GXI = (String.IsNullOrEmpty(StringUtil.RTrim( A40000TeamContryFlag_GXI))&&String.IsNullOrEmpty(StringUtil.RTrim( A30TeamContryFlag)) ? true : false);
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               A14Teamid = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Teamid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14Teamid), 4, 0)));
               getEqualNoModal( ) ;
               Gx_mode = "DSP";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons_dsp( ) ;
               standaloneModal( ) ;
            }
            else
            {
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               standaloneModal( ) ;
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll055( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
         }
         DisableAttributes055( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_058( )
      {
         nGXsfl_68_idx = 0;
         while ( nGXsfl_68_idx < nRC_GXsfl_68 )
         {
            ReadRow058( ) ;
            if ( ( nRcdExists_8 != 0 ) || ( nIsMod_8 != 0 ) )
            {
               GetKey058( ) ;
               if ( ( nRcdExists_8 == 0 ) && ( nRcdDeleted_8 == 0 ) )
               {
                  if ( RcdFound8 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     BeforeValidate058( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable058( ) ;
                        CloseExtendedTableCursors058( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                     }
                  }
                  else
                  {
                     GXCCtl = "ATHLETEID_" + sGXsfl_68_idx;
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, GXCCtl);
                     AnyError = 1;
                     GX_FocusControl = edtAthleteId_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound8 != 0 )
                  {
                     if ( nRcdDeleted_8 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        getByPrimaryKey058( ) ;
                        Load058( ) ;
                        BeforeValidate058( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls058( ) ;
                        }
                     }
                     else
                     {
                        if ( nIsMod_8 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           BeforeValidate058( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable058( ) ;
                              CloseExtendedTableCursors058( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_8 == 0 )
                     {
                        GXCCtl = "ATHLETEID_" + sGXsfl_68_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtAthleteId_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtAthleteId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A6AthleteId), 4, 0, ".", ""))) ;
            ChangePostValue( edtAthlete_Name_Internalname, StringUtil.RTrim( A29Athlete_Name)) ;
            ChangePostValue( edtCountryId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A3CountryId), 4, 0, ".", ""))) ;
            ChangePostValue( edtCountryName_Internalname, StringUtil.RTrim( A4CountryName)) ;
            ChangePostValue( "ZT_"+"Z6AthleteId_"+sGXsfl_68_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z6AthleteId), 4, 0, ".", ""))) ;
            ChangePostValue( "nRcdDeleted_8_"+sGXsfl_68_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_8), 4, 0, ".", ""))) ;
            ChangePostValue( "nRcdExists_8_"+sGXsfl_68_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_8), 4, 0, ".", ""))) ;
            ChangePostValue( "nIsMod_8_"+sGXsfl_68_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_8), 4, 0, ".", ""))) ;
            if ( nIsMod_8 != 0 )
            {
               ChangePostValue( "ATHLETEID_"+sGXsfl_68_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAthleteId_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "ATHLETE_NAME_"+sGXsfl_68_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAthlete_Name_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "COUNTRYID_"+sGXsfl_68_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCountryId_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "COUNTRYNAME_"+sGXsfl_68_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCountryName_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption050( )
      {
      }

      protected void ZM055( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1DisciplineId = T00057_A1DisciplineId[0];
            }
            else
            {
               Z1DisciplineId = A1DisciplineId;
            }
         }
         if ( GX_JID == -1 )
         {
            Z14Teamid = A14Teamid;
            Z40000TeamContryFlag_GXI = A40000TeamContryFlag_GXI;
            Z1DisciplineId = A1DisciplineId;
            Z2DisciplineName = A2DisciplineName;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_1_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0010.aspx"+"',["+"{Ctrl:gx.dom.el('"+"DISCIPLINEID"+"'), id:'"+"DISCIPLINEID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
      }

      protected void Load055( )
      {
         /* Using cursor T00059 */
         pr_default.execute(7, new Object[] {A14Teamid});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound5 = 1;
            A40000TeamContryFlag_GXI = T00059_A40000TeamContryFlag_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTeamContryFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A30TeamContryFlag)) ? A40000TeamContryFlag_GXI : context.convertURL( context.PathToRelativeUrl( A30TeamContryFlag))), true);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTeamContryFlag_Internalname, "SrcSet", context.GetImageSrcSet( A30TeamContryFlag), true);
            A2DisciplineName = T00059_A2DisciplineName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2DisciplineName", A2DisciplineName);
            A1DisciplineId = T00059_A1DisciplineId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1DisciplineId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1DisciplineId), 4, 0)));
            ZM055( -1) ;
         }
         pr_default.close(7);
         OnLoadActions055( ) ;
      }

      protected void OnLoadActions055( )
      {
      }

      protected void CheckExtendedTable055( )
      {
         nIsDirty_5 = 0;
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00058 */
         pr_default.execute(6, new Object[] {A1DisciplineId});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("No matching 'Discipline'.", "ForeignKeyNotFound", 1, "DISCIPLINEID");
            AnyError = 1;
            GX_FocusControl = edtDisciplineId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2DisciplineName = T00058_A2DisciplineName[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2DisciplineName", A2DisciplineName);
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors055( )
      {
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( short A1DisciplineId )
      {
         /* Using cursor T000510 */
         pr_default.execute(8, new Object[] {A1DisciplineId});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("No matching 'Discipline'.", "ForeignKeyNotFound", 1, "DISCIPLINEID");
            AnyError = 1;
            GX_FocusControl = edtDisciplineId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2DisciplineName = T000510_A2DisciplineName[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2DisciplineName", A2DisciplineName);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2DisciplineName))+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_default.close(8);
      }

      protected void GetKey055( )
      {
         /* Using cursor T000511 */
         pr_default.execute(9, new Object[] {A14Teamid});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound5 = 1;
         }
         else
         {
            RcdFound5 = 0;
         }
         pr_default.close(9);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00057 */
         pr_default.execute(5, new Object[] {A14Teamid});
         if ( (pr_default.getStatus(5) != 101) )
         {
            ZM055( 1) ;
            RcdFound5 = 1;
            A14Teamid = T00057_A14Teamid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Teamid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14Teamid), 4, 0)));
            A40000TeamContryFlag_GXI = T00057_A40000TeamContryFlag_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTeamContryFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A30TeamContryFlag)) ? A40000TeamContryFlag_GXI : context.convertURL( context.PathToRelativeUrl( A30TeamContryFlag))), true);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTeamContryFlag_Internalname, "SrcSet", context.GetImageSrcSet( A30TeamContryFlag), true);
            A1DisciplineId = T00057_A1DisciplineId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1DisciplineId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1DisciplineId), 4, 0)));
            Z14Teamid = A14Teamid;
            sMode5 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load055( ) ;
            if ( AnyError == 1 )
            {
               RcdFound5 = 0;
               InitializeNonKey055( ) ;
            }
            Gx_mode = sMode5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound5 = 0;
            InitializeNonKey055( ) ;
            sMode5 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(5);
      }

      protected void getEqualNoModal( )
      {
         GetKey055( ) ;
         if ( RcdFound5 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound5 = 0;
         /* Using cursor T000512 */
         pr_default.execute(10, new Object[] {A14Teamid});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T000512_A14Teamid[0] < A14Teamid ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T000512_A14Teamid[0] > A14Teamid ) ) )
            {
               A14Teamid = T000512_A14Teamid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Teamid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14Teamid), 4, 0)));
               RcdFound5 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void move_previous( )
      {
         RcdFound5 = 0;
         /* Using cursor T000513 */
         pr_default.execute(11, new Object[] {A14Teamid});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T000513_A14Teamid[0] > A14Teamid ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T000513_A14Teamid[0] < A14Teamid ) ) )
            {
               A14Teamid = T000513_A14Teamid[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Teamid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14Teamid), 4, 0)));
               RcdFound5 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey055( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtTeamid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert055( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound5 == 1 )
            {
               if ( A14Teamid != Z14Teamid )
               {
                  A14Teamid = Z14Teamid;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Teamid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14Teamid), 4, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "TEAMID");
                  AnyError = 1;
                  GX_FocusControl = edtTeamid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtTeamid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update055( ) ;
                  GX_FocusControl = edtTeamid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A14Teamid != Z14Teamid )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtTeamid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert055( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "TEAMID");
                     AnyError = 1;
                     GX_FocusControl = edtTeamid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtTeamid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert055( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A14Teamid != Z14Teamid )
         {
            A14Teamid = Z14Teamid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Teamid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14Teamid), 4, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "TEAMID");
            AnyError = 1;
            GX_FocusControl = edtTeamid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtTeamid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound5 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "TEAMID");
            AnyError = 1;
            GX_FocusControl = edtTeamid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtDisciplineId_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart055( ) ;
         if ( RcdFound5 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtDisciplineId_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd055( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound5 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtDisciplineId_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound5 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtDisciplineId_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart055( ) ;
         if ( RcdFound5 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound5 != 0 )
            {
               ScanNext055( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtDisciplineId_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd055( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency055( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00056 */
            pr_default.execute(4, new Object[] {A14Teamid});
            if ( (pr_default.getStatus(4) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Team"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(4) == 101) || ( Z1DisciplineId != T00056_A1DisciplineId[0] ) )
            {
               if ( Z1DisciplineId != T00056_A1DisciplineId[0] )
               {
                  GXUtil.WriteLog("team:[seudo value changed for attri]"+"DisciplineId");
                  GXUtil.WriteLogRaw("Old: ",Z1DisciplineId);
                  GXUtil.WriteLogRaw("Current: ",T00056_A1DisciplineId[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Team"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert055( )
      {
         BeforeValidate055( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable055( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM055( 0) ;
            CheckOptimisticConcurrency055( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm055( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert055( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000514 */
                     pr_default.execute(12, new Object[] {A40000TeamContryFlag_GXI, A1DisciplineId});
                     A14Teamid = T000514_A14Teamid[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Teamid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14Teamid), 4, 0)));
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("Team") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel055( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                              ResetCaption050( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load055( ) ;
            }
            EndLevel055( ) ;
         }
         CloseExtendedTableCursors055( ) ;
      }

      protected void Update055( )
      {
         BeforeValidate055( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable055( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency055( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm055( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate055( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000515 */
                     pr_default.execute(13, new Object[] {A1DisciplineId, A14Teamid});
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("Team") ;
                     if ( (pr_default.getStatus(13) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Team"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate055( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel055( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                              ResetCaption050( ) ;
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel055( ) ;
         }
         CloseExtendedTableCursors055( ) ;
      }

      protected void DeferredUpdate055( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate055( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency055( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls055( ) ;
            AfterConfirm055( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete055( ) ;
               if ( AnyError == 0 )
               {
                  ScanStart058( ) ;
                  while ( RcdFound8 != 0 )
                  {
                     getByPrimaryKey058( ) ;
                     Delete058( ) ;
                     ScanNext058( ) ;
                  }
                  ScanEnd058( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000516 */
                     pr_default.execute(14, new Object[] {A14Teamid});
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("Team") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           move_next( ) ;
                           if ( RcdFound5 == 0 )
                           {
                              InitAll055( ) ;
                              Gx_mode = "INS";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           }
                           else
                           {
                              getByPrimaryKey( ) ;
                              Gx_mode = "UPD";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           }
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                           ResetCaption050( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode5 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel055( ) ;
         Gx_mode = sMode5;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls055( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000517 */
            pr_default.execute(15, new Object[] {A1DisciplineId});
            A2DisciplineName = T000517_A2DisciplineName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2DisciplineName", A2DisciplineName);
            pr_default.close(15);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000518 */
            pr_default.execute(16, new Object[] {A14Teamid});
            if ( (pr_default.getStatus(16) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Team"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(16);
         }
      }

      protected void ProcessNestedLevel058( )
      {
         nGXsfl_68_idx = 0;
         while ( nGXsfl_68_idx < nRC_GXsfl_68 )
         {
            ReadRow058( ) ;
            if ( ( nRcdExists_8 != 0 ) || ( nIsMod_8 != 0 ) )
            {
               standaloneNotModal058( ) ;
               GetKey058( ) ;
               if ( ( nRcdExists_8 == 0 ) && ( nRcdDeleted_8 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  Insert058( ) ;
               }
               else
               {
                  if ( RcdFound8 != 0 )
                  {
                     if ( ( nRcdDeleted_8 != 0 ) && ( nRcdExists_8 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        Delete058( ) ;
                     }
                     else
                     {
                        if ( nRcdExists_8 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           Update058( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_8 == 0 )
                     {
                        GXCCtl = "ATHLETEID_" + sGXsfl_68_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtAthleteId_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtAthleteId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A6AthleteId), 4, 0, ".", ""))) ;
            ChangePostValue( edtAthlete_Name_Internalname, StringUtil.RTrim( A29Athlete_Name)) ;
            ChangePostValue( edtCountryId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A3CountryId), 4, 0, ".", ""))) ;
            ChangePostValue( edtCountryName_Internalname, StringUtil.RTrim( A4CountryName)) ;
            ChangePostValue( "ZT_"+"Z6AthleteId_"+sGXsfl_68_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z6AthleteId), 4, 0, ".", ""))) ;
            ChangePostValue( "nRcdDeleted_8_"+sGXsfl_68_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_8), 4, 0, ".", ""))) ;
            ChangePostValue( "nRcdExists_8_"+sGXsfl_68_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_8), 4, 0, ".", ""))) ;
            ChangePostValue( "nIsMod_8_"+sGXsfl_68_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_8), 4, 0, ".", ""))) ;
            if ( nIsMod_8 != 0 )
            {
               ChangePostValue( "ATHLETEID_"+sGXsfl_68_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAthleteId_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "ATHLETE_NAME_"+sGXsfl_68_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAthlete_Name_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "COUNTRYID_"+sGXsfl_68_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCountryId_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "COUNTRYNAME_"+sGXsfl_68_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCountryName_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll058( ) ;
         if ( AnyError != 0 )
         {
         }
         nRcdExists_8 = 0;
         nIsMod_8 = 0;
         nRcdDeleted_8 = 0;
      }

      protected void ProcessLevel055( )
      {
         /* Save parent mode. */
         sMode5 = Gx_mode;
         ProcessNestedLevel058( ) ;
         if ( AnyError != 0 )
         {
         }
         /* Restore parent mode. */
         Gx_mode = sMode5;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         /* ' Update level parameters */
      }

      protected void EndLevel055( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(4);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete055( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(5);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(15);
            pr_default.close(2);
            pr_default.close(3);
            context.CommitDataStores("team",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues050( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(5);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(15);
            pr_default.close(2);
            pr_default.close(3);
            context.RollbackDataStores("team",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart055( )
      {
         /* Using cursor T000519 */
         pr_default.execute(17);
         RcdFound5 = 0;
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound5 = 1;
            A14Teamid = T000519_A14Teamid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Teamid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14Teamid), 4, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext055( )
      {
         /* Scan next routine */
         pr_default.readNext(17);
         RcdFound5 = 0;
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound5 = 1;
            A14Teamid = T000519_A14Teamid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Teamid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14Teamid), 4, 0)));
         }
      }

      protected void ScanEnd055( )
      {
         pr_default.close(17);
      }

      protected void AfterConfirm055( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert055( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate055( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete055( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete055( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate055( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes055( )
      {
         edtTeamid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTeamid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTeamid_Enabled), 5, 0)), true);
         edtTeamCountryId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTeamCountryId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTeamCountryId_Enabled), 5, 0)), true);
         edtTeamCountryName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTeamCountryName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTeamCountryName_Enabled), 5, 0)), true);
         imgTeamContryFlag_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTeamContryFlag_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgTeamContryFlag_Enabled), 5, 0)), true);
         edtDisciplineId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtDisciplineId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtDisciplineId_Enabled), 5, 0)), true);
         edtDisciplineName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtDisciplineName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtDisciplineName_Enabled), 5, 0)), true);
      }

      protected void ZM058( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -3 )
         {
            Z14Teamid = A14Teamid;
            Z6AthleteId = A6AthleteId;
            Z29Athlete_Name = A29Athlete_Name;
            Z3CountryId = A3CountryId;
            Z4CountryName = A4CountryName;
         }
      }

      protected void standaloneNotModal058( )
      {
      }

      protected void standaloneModal058( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            edtAthleteId_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAthleteId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAthleteId_Enabled), 5, 0)), !bGXsfl_68_Refreshing);
         }
         else
         {
            edtAthleteId_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAthleteId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAthleteId_Enabled), 5, 0)), !bGXsfl_68_Refreshing);
         }
      }

      protected void Load058( )
      {
         /* Using cursor T000520 */
         pr_default.execute(18, new Object[] {A14Teamid, A6AthleteId});
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound8 = 1;
            A29Athlete_Name = T000520_A29Athlete_Name[0];
            A4CountryName = T000520_A4CountryName[0];
            A3CountryId = T000520_A3CountryId[0];
            ZM058( -3) ;
         }
         pr_default.close(18);
         OnLoadActions058( ) ;
      }

      protected void OnLoadActions058( )
      {
      }

      protected void CheckExtendedTable058( )
      {
         nIsDirty_8 = 0;
         Gx_BScreen = 1;
         standaloneModal058( ) ;
         /* Using cursor T00054 */
         pr_default.execute(2, new Object[] {A6AthleteId});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GXCCtl = "ATHLETEID_" + sGXsfl_68_idx;
            GX_msglist.addItem("No matching 'Athlete'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtAthleteId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A29Athlete_Name = T00054_A29Athlete_Name[0];
         A3CountryId = T00054_A3CountryId[0];
         pr_default.close(2);
         /* Using cursor T00055 */
         pr_default.execute(3, new Object[] {A3CountryId});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("No matching 'Country'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A4CountryName = T00055_A4CountryName[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors058( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable058( )
      {
      }

      protected void gxLoad_4( short A6AthleteId )
      {
         /* Using cursor T000521 */
         pr_default.execute(19, new Object[] {A6AthleteId});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GXCCtl = "ATHLETEID_" + sGXsfl_68_idx;
            GX_msglist.addItem("No matching 'Athlete'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtAthleteId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A29Athlete_Name = T000521_A29Athlete_Name[0];
         A3CountryId = T000521_A3CountryId[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A29Athlete_Name))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A3CountryId), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(19) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_default.close(19);
      }

      protected void gxLoad_5( short A3CountryId )
      {
         /* Using cursor T000522 */
         pr_default.execute(20, new Object[] {A3CountryId});
         if ( (pr_default.getStatus(20) == 101) )
         {
            GX_msglist.addItem("No matching 'Country'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A4CountryName = T000522_A4CountryName[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A4CountryName))+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(20) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_default.close(20);
      }

      protected void GetKey058( )
      {
         /* Using cursor T000523 */
         pr_default.execute(21, new Object[] {A14Teamid, A6AthleteId});
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound8 = 1;
         }
         else
         {
            RcdFound8 = 0;
         }
         pr_default.close(21);
      }

      protected void getByPrimaryKey058( )
      {
         /* Using cursor T00053 */
         pr_default.execute(1, new Object[] {A14Teamid, A6AthleteId});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM058( 3) ;
            RcdFound8 = 1;
            InitializeNonKey058( ) ;
            A6AthleteId = T00053_A6AthleteId[0];
            Z14Teamid = A14Teamid;
            Z6AthleteId = A6AthleteId;
            sMode8 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal058( ) ;
            Load058( ) ;
            Gx_mode = sMode8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound8 = 0;
            InitializeNonKey058( ) ;
            sMode8 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal058( ) ;
            Gx_mode = sMode8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes058( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency058( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00052 */
            pr_default.execute(0, new Object[] {A14Teamid, A6AthleteId});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"TeamAthlete"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"TeamAthlete"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert058( )
      {
         BeforeValidate058( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable058( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM058( 0) ;
            CheckOptimisticConcurrency058( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm058( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert058( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000524 */
                     pr_default.execute(22, new Object[] {A14Teamid, A6AthleteId});
                     pr_default.close(22);
                     dsDefault.SmartCacheProvider.SetUpdated("TeamAthlete") ;
                     if ( (pr_default.getStatus(22) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load058( ) ;
            }
            EndLevel058( ) ;
         }
         CloseExtendedTableCursors058( ) ;
      }

      protected void Update058( )
      {
         BeforeValidate058( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable058( ) ;
         }
         if ( ( nIsMod_8 != 0 ) || ( nIsDirty_8 != 0 ) )
         {
            if ( AnyError == 0 )
            {
               CheckOptimisticConcurrency058( ) ;
               if ( AnyError == 0 )
               {
                  AfterConfirm058( ) ;
                  if ( AnyError == 0 )
                  {
                     BeforeUpdate058( ) ;
                     if ( AnyError == 0 )
                     {
                        /* No attributes to update on table [TeamAthlete] */
                        DeferredUpdate058( ) ;
                        if ( AnyError == 0 )
                        {
                           /* Start of After( update) rules */
                           /* End of After( update) rules */
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey058( ) ;
                           }
                        }
                        else
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                           AnyError = 1;
                        }
                     }
                  }
               }
               EndLevel058( ) ;
            }
         }
         CloseExtendedTableCursors058( ) ;
      }

      protected void DeferredUpdate058( )
      {
      }

      protected void Delete058( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate058( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency058( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls058( ) ;
            AfterConfirm058( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete058( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000525 */
                  pr_default.execute(23, new Object[] {A14Teamid, A6AthleteId});
                  pr_default.close(23);
                  dsDefault.SmartCacheProvider.SetUpdated("TeamAthlete") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode8 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel058( ) ;
         Gx_mode = sMode8;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls058( )
      {
         standaloneModal058( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000526 */
            pr_default.execute(24, new Object[] {A6AthleteId});
            A29Athlete_Name = T000526_A29Athlete_Name[0];
            A3CountryId = T000526_A3CountryId[0];
            pr_default.close(24);
            /* Using cursor T000527 */
            pr_default.execute(25, new Object[] {A3CountryId});
            A4CountryName = T000527_A4CountryName[0];
            pr_default.close(25);
         }
      }

      protected void EndLevel058( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart058( )
      {
         /* Scan By routine */
         /* Using cursor T000528 */
         pr_default.execute(26, new Object[] {A14Teamid});
         RcdFound8 = 0;
         if ( (pr_default.getStatus(26) != 101) )
         {
            RcdFound8 = 1;
            A6AthleteId = T000528_A6AthleteId[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext058( )
      {
         /* Scan next routine */
         pr_default.readNext(26);
         RcdFound8 = 0;
         if ( (pr_default.getStatus(26) != 101) )
         {
            RcdFound8 = 1;
            A6AthleteId = T000528_A6AthleteId[0];
         }
      }

      protected void ScanEnd058( )
      {
         pr_default.close(26);
      }

      protected void AfterConfirm058( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert058( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate058( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete058( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete058( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate058( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes058( )
      {
         edtAthleteId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAthleteId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAthleteId_Enabled), 5, 0)), !bGXsfl_68_Refreshing);
         edtAthlete_Name_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAthlete_Name_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAthlete_Name_Enabled), 5, 0)), !bGXsfl_68_Refreshing);
         edtCountryId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCountryId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCountryId_Enabled), 5, 0)), !bGXsfl_68_Refreshing);
         edtCountryName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCountryName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCountryName_Enabled), 5, 0)), !bGXsfl_68_Refreshing);
      }

      protected void send_integrity_lvl_hashes058( )
      {
      }

      protected void send_integrity_lvl_hashes055( )
      {
      }

      protected void SubsflControlProps_688( )
      {
         edtAthleteId_Internalname = "ATHLETEID_"+sGXsfl_68_idx;
         imgprompt_6_Internalname = "PROMPT_6_"+sGXsfl_68_idx;
         edtAthlete_Name_Internalname = "ATHLETE_NAME_"+sGXsfl_68_idx;
         edtCountryId_Internalname = "COUNTRYID_"+sGXsfl_68_idx;
         edtCountryName_Internalname = "COUNTRYNAME_"+sGXsfl_68_idx;
      }

      protected void SubsflControlProps_fel_688( )
      {
         edtAthleteId_Internalname = "ATHLETEID_"+sGXsfl_68_fel_idx;
         imgprompt_6_Internalname = "PROMPT_6_"+sGXsfl_68_fel_idx;
         edtAthlete_Name_Internalname = "ATHLETE_NAME_"+sGXsfl_68_fel_idx;
         edtCountryId_Internalname = "COUNTRYID_"+sGXsfl_68_fel_idx;
         edtCountryName_Internalname = "COUNTRYNAME_"+sGXsfl_68_fel_idx;
      }

      protected void AddRow058( )
      {
         nGXsfl_68_idx = (int)(nGXsfl_68_idx+1);
         sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
         SubsflControlProps_688( ) ;
         SendRow058( ) ;
      }

      protected void SendRow058( )
      {
         Gridteam_athleteRow = GXWebRow.GetNew(context);
         if ( subGridteam_athlete_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridteam_athlete_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridteam_athlete_Class, "") != 0 )
            {
               subGridteam_athlete_Linesclass = subGridteam_athlete_Class+"Odd";
            }
         }
         else if ( subGridteam_athlete_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridteam_athlete_Backstyle = 0;
            subGridteam_athlete_Backcolor = subGridteam_athlete_Allbackcolor;
            if ( StringUtil.StrCmp(subGridteam_athlete_Class, "") != 0 )
            {
               subGridteam_athlete_Linesclass = subGridteam_athlete_Class+"Uniform";
            }
         }
         else if ( subGridteam_athlete_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridteam_athlete_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridteam_athlete_Class, "") != 0 )
            {
               subGridteam_athlete_Linesclass = subGridteam_athlete_Class+"Odd";
            }
            subGridteam_athlete_Backcolor = (int)(0x0);
         }
         else if ( subGridteam_athlete_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridteam_athlete_Backstyle = 1;
            if ( ((int)((nGXsfl_68_idx) % (2))) == 0 )
            {
               subGridteam_athlete_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridteam_athlete_Class, "") != 0 )
               {
                  subGridteam_athlete_Linesclass = subGridteam_athlete_Class+"Even";
               }
            }
            else
            {
               subGridteam_athlete_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridteam_athlete_Class, "") != 0 )
               {
                  subGridteam_athlete_Linesclass = subGridteam_athlete_Class+"Odd";
               }
            }
         }
         imgprompt_6_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0)||(StringUtil.StrCmp(Gx_mode, "UPD")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0030.aspx"+"',["+"{Ctrl:gx.dom.el('"+"ATHLETEID_"+sGXsfl_68_idx+"'), id:'"+"ATHLETEID_"+sGXsfl_68_idx+"'"+",IOType:'out'}"+"],"+"gx.dom.form()."+"nIsMod_8_"+sGXsfl_68_idx+","+"'', false"+","+"false"+");");
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_8_" + sGXsfl_68_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_68_idx + "',68)\"";
         ROClassString = "Attribute";
         Gridteam_athleteRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAthleteId_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A6AthleteId), 4, 0, ".", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(A6AthleteId), "ZZZ9")),TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,69);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAthleteId_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtAthleteId_Enabled,(short)1,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)0,(bool)true,(String)"Id",(String)"right",(bool)false});
         /* Subfile cell */
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         Gridteam_athleteRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)imgprompt_6_Internalname,(String)sImgUrl,(String)imgprompt_6_Link,(String)"",(String)"",context.GetTheme( ),(int)imgprompt_6_Visible,(short)1,(String)"",(String)"",(short)0,(short)0,(short)0,(String)"",(short)0,(String)"",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)false,(bool)false,context.GetImageSrcSet( sImgUrl)});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "Attribute";
         Gridteam_athleteRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAthlete_Name_Internalname,StringUtil.RTrim( A29Athlete_Name),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAthlete_Name_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtAthlete_Name_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)60,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)-1,(bool)true,(String)"Name",(String)"left",(bool)true});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "Attribute";
         Gridteam_athleteRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCountryId_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A3CountryId), 4, 0, ".", "")),((edtCountryId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A3CountryId), "ZZZ9")) : context.localUtil.Format( (decimal)(A3CountryId), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCountryId_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtCountryId_Enabled,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)0,(bool)true,(String)"Id",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "Attribute";
         Gridteam_athleteRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCountryName_Internalname,StringUtil.RTrim( A4CountryName),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCountryName_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtCountryName_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)60,(short)0,(short)0,(short)68,(short)1,(short)-1,(short)-1,(bool)true,(String)"Name",(String)"left",(bool)true});
         context.httpAjaxContext.ajax_sending_grid_row(Gridteam_athleteRow);
         send_integrity_lvl_hashes058( ) ;
         GXCCtl = "Z6AthleteId_" + sGXsfl_68_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z6AthleteId), 4, 0, ".", "")));
         GXCCtl = "nRcdDeleted_8_" + sGXsfl_68_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_8), 4, 0, ".", "")));
         GXCCtl = "nRcdExists_8_" + sGXsfl_68_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_8), 4, 0, ".", "")));
         GXCCtl = "nIsMod_8_" + sGXsfl_68_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_8), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "ATHLETEID_"+sGXsfl_68_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAthleteId_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "ATHLETE_NAME_"+sGXsfl_68_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAthlete_Name_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "COUNTRYID_"+sGXsfl_68_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCountryId_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "COUNTRYNAME_"+sGXsfl_68_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCountryName_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "PROMPT_6_"+sGXsfl_68_idx+"Link", StringUtil.RTrim( imgprompt_6_Link));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Gridteam_athleteContainer.AddRow(Gridteam_athleteRow);
      }

      protected void ReadRow058( )
      {
         nGXsfl_68_idx = (int)(nGXsfl_68_idx+1);
         sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
         SubsflControlProps_688( ) ;
         edtAthleteId_Enabled = (int)(context.localUtil.CToN( cgiGet( "ATHLETEID_"+sGXsfl_68_idx+"Enabled"), ".", ","));
         edtAthlete_Name_Enabled = (int)(context.localUtil.CToN( cgiGet( "ATHLETE_NAME_"+sGXsfl_68_idx+"Enabled"), ".", ","));
         edtCountryId_Enabled = (int)(context.localUtil.CToN( cgiGet( "COUNTRYID_"+sGXsfl_68_idx+"Enabled"), ".", ","));
         edtCountryName_Enabled = (int)(context.localUtil.CToN( cgiGet( "COUNTRYNAME_"+sGXsfl_68_idx+"Enabled"), ".", ","));
         imgprompt_1_Link = cgiGet( "PROMPT_6_"+sGXsfl_68_idx+"Link");
         if ( ( ( context.localUtil.CToN( cgiGet( edtAthleteId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAthleteId_Internalname), ".", ",") > Convert.ToDecimal( 9999 )) ) )
         {
            GXCCtl = "ATHLETEID_" + sGXsfl_68_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtAthleteId_Internalname;
            wbErr = true;
            A6AthleteId = 0;
         }
         else
         {
            A6AthleteId = (short)(context.localUtil.CToN( cgiGet( edtAthleteId_Internalname), ".", ","));
         }
         A29Athlete_Name = cgiGet( edtAthlete_Name_Internalname);
         A3CountryId = (short)(context.localUtil.CToN( cgiGet( edtCountryId_Internalname), ".", ","));
         A4CountryName = cgiGet( edtCountryName_Internalname);
         GXCCtl = "Z6AthleteId_" + sGXsfl_68_idx;
         Z6AthleteId = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "nRcdDeleted_8_" + sGXsfl_68_idx;
         nRcdDeleted_8 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "nRcdExists_8_" + sGXsfl_68_idx;
         nRcdExists_8 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "nIsMod_8_" + sGXsfl_68_idx;
         nIsMod_8 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
      }

      protected void assign_properties_default( )
      {
         defedtAthleteId_Enabled = edtAthleteId_Enabled;
      }

      protected void ConfirmValues050( )
      {
         nGXsfl_68_idx = 0;
         sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
         SubsflControlProps_688( ) ;
         while ( nGXsfl_68_idx < nRC_GXsfl_68 )
         {
            nGXsfl_68_idx = (int)(nGXsfl_68_idx+1);
            sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
            SubsflControlProps_688( ) ;
            ChangePostValue( "Z6AthleteId_"+sGXsfl_68_idx, cgiGet( "ZT_"+"Z6AthleteId_"+sGXsfl_68_idx)) ;
            DeletePostValue( "ZT_"+"Z6AthleteId_"+sGXsfl_68_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 135614), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("gxcfg.js", "?20226154143438", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("team.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z14Teamid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z14Teamid), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z1DisciplineId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1DisciplineId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_68", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_68_idx), 8, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "TEAMCONTRYFLAG_GXI", A40000TeamContryFlag_GXI);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXCCtlgxBlob = "TEAMCONTRYFLAG" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A30TeamContryFlag);
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("team.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Team" ;
      }

      public override String GetPgmdesc( )
      {
         return "Team" ;
      }

      protected void InitializeNonKey055( )
      {
         A16TeamCountryId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A16TeamCountryId", StringUtil.LTrim( StringUtil.Str( (decimal)(A16TeamCountryId), 4, 0)));
         A17TeamCountryName = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17TeamCountryName", A17TeamCountryName);
         A30TeamContryFlag = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30TeamContryFlag", A30TeamContryFlag);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTeamContryFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A30TeamContryFlag)) ? A40000TeamContryFlag_GXI : context.convertURL( context.PathToRelativeUrl( A30TeamContryFlag))), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTeamContryFlag_Internalname, "SrcSet", context.GetImageSrcSet( A30TeamContryFlag), true);
         A40000TeamContryFlag_GXI = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTeamContryFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A30TeamContryFlag)) ? A40000TeamContryFlag_GXI : context.convertURL( context.PathToRelativeUrl( A30TeamContryFlag))), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgTeamContryFlag_Internalname, "SrcSet", context.GetImageSrcSet( A30TeamContryFlag), true);
         A1DisciplineId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1DisciplineId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1DisciplineId), 4, 0)));
         A2DisciplineName = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2DisciplineName", A2DisciplineName);
         Z1DisciplineId = 0;
      }

      protected void InitAll055( )
      {
         A14Teamid = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Teamid", StringUtil.LTrim( StringUtil.Str( (decimal)(A14Teamid), 4, 0)));
         InitializeNonKey055( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey058( )
      {
         A29Athlete_Name = "";
         A3CountryId = 0;
         A4CountryName = "";
      }

      protected void InitAll058( )
      {
         A6AthleteId = 0;
         InitializeNonKey058( ) ;
      }

      protected void StandaloneModalInsert058( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20226154143444", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("team.js", "?20226154143445", false, true);
         /* End function include_jscripts */
      }

      protected void init_level_properties8( )
      {
         edtAthleteId_Enabled = defedtAthleteId_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAthleteId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAthleteId_Enabled), 5, 0)), !bGXsfl_68_Refreshing);
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtTeamid_Internalname = "TEAMID";
         edtTeamCountryId_Internalname = "TEAMCOUNTRYID";
         edtTeamCountryName_Internalname = "TEAMCOUNTRYNAME";
         imgTeamContryFlag_Internalname = "TEAMCONTRYFLAG";
         edtDisciplineId_Internalname = "DISCIPLINEID";
         edtDisciplineName_Internalname = "DISCIPLINENAME";
         lblTitleathlete_Internalname = "TITLEATHLETE";
         edtAthleteId_Internalname = "ATHLETEID";
         edtAthlete_Name_Internalname = "ATHLETE_NAME";
         edtCountryId_Internalname = "COUNTRYID";
         edtCountryName_Internalname = "COUNTRYNAME";
         divAthletetable_Internalname = "ATHLETETABLE";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_1_Internalname = "PROMPT_1";
         imgprompt_6_Internalname = "PROMPT_6";
         subGridteam_athlete_Internalname = "GRIDTEAM_ATHLETE";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Team";
         edtCountryName_Jsonclick = "";
         edtCountryId_Jsonclick = "";
         edtAthlete_Name_Jsonclick = "";
         imgprompt_6_Visible = 1;
         imgprompt_6_Link = "";
         imgprompt_1_Visible = 1;
         edtAthleteId_Jsonclick = "";
         subGridteam_athlete_Class = "Grid";
         subGridteam_athlete_Backcolorstyle = 0;
         subGridteam_athlete_Allowcollapsing = 0;
         subGridteam_athlete_Allowselection = 0;
         edtCountryName_Enabled = 0;
         edtCountryId_Enabled = 0;
         edtAthlete_Name_Enabled = 0;
         edtAthleteId_Enabled = 1;
         subGridteam_athlete_Header = "";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtDisciplineName_Jsonclick = "";
         edtDisciplineName_Enabled = 0;
         imgprompt_1_Visible = 1;
         imgprompt_1_Link = "";
         edtDisciplineId_Jsonclick = "";
         edtDisciplineId_Enabled = 1;
         imgTeamContryFlag_Enabled = 0;
         edtTeamCountryName_Jsonclick = "";
         edtTeamCountryName_Enabled = 0;
         edtTeamCountryId_Jsonclick = "";
         edtTeamCountryId_Enabled = 0;
         edtTeamid_Jsonclick = "";
         edtTeamid_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridteam_athlete_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         SubsflControlProps_688( ) ;
         while ( nGXsfl_68_idx <= nRC_GXsfl_68 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal058( ) ;
            standaloneModal058( ) ;
            init_web_controls( ) ;
            dynload_actions( ) ;
            SendRow058( ) ;
            nGXsfl_68_idx = (int)(nGXsfl_68_idx+1);
            sGXsfl_68_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_68_idx), 4, 0)), 4, "0");
            SubsflControlProps_688( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( Gridteam_athleteContainer));
         /* End function gxnrGridteam_athlete_newrow */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtDisciplineId_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Teamid( )
      {
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A16TeamCountryId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A16TeamCountryId), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17TeamCountryName", StringUtil.RTrim( A17TeamCountryName));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30TeamContryFlag", context.PathToRelativeUrl( A30TeamContryFlag));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40000TeamContryFlag_GXI", A40000TeamContryFlag_GXI);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1DisciplineId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1DisciplineId), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2DisciplineName", StringUtil.RTrim( A2DisciplineName));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "Z14Teamid", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z14Teamid), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z16TeamCountryId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z16TeamCountryId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z17TeamCountryName", StringUtil.RTrim( Z17TeamCountryName));
         GxWebStd.gx_hidden_field( context, "Z30TeamContryFlag", context.PathToRelativeUrl( Z30TeamContryFlag));
         GxWebStd.gx_hidden_field( context, "Z40000TeamContryFlag_GXI", Z40000TeamContryFlag_GXI);
         GxWebStd.gx_hidden_field( context, "Z1DisciplineId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1DisciplineId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z2DisciplineName", StringUtil.RTrim( Z2DisciplineName));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         SendCloseFormHiddens( ) ;
      }

      public void Valid_Disciplineid( )
      {
         /* Using cursor T000517 */
         pr_default.execute(15, new Object[] {A1DisciplineId});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("No matching 'Discipline'.", "ForeignKeyNotFound", 1, "DISCIPLINEID");
            AnyError = 1;
            GX_FocusControl = edtDisciplineId_Internalname;
         }
         A2DisciplineName = T000517_A2DisciplineName[0];
         pr_default.close(15);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2DisciplineName", StringUtil.RTrim( A2DisciplineName));
      }

      public void Valid_Athleteid( )
      {
         /* Using cursor T000526 */
         pr_default.execute(24, new Object[] {A6AthleteId});
         if ( (pr_default.getStatus(24) == 101) )
         {
            GX_msglist.addItem("No matching 'Athlete'.", "ForeignKeyNotFound", 1, "ATHLETEID");
            AnyError = 1;
            GX_FocusControl = edtAthleteId_Internalname;
         }
         A29Athlete_Name = T000526_A29Athlete_Name[0];
         A3CountryId = T000526_A3CountryId[0];
         pr_default.close(24);
         /* Using cursor T000527 */
         pr_default.execute(25, new Object[] {A3CountryId});
         if ( (pr_default.getStatus(25) == 101) )
         {
            GX_msglist.addItem("No matching 'Country'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A4CountryName = T000527_A4CountryName[0];
         pr_default.close(25);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Athlete_Name", StringUtil.RTrim( A29Athlete_Name));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3CountryId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3CountryId), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4CountryName", StringUtil.RTrim( A4CountryName));
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("VALID_TEAMID","{handler:'Valid_Teamid',iparms:[{av:'A14Teamid',fld:'TEAMID',pic:'ZZZ9'},{av:'Gx_mode',fld:'vMODE',pic:'@!'}]");
         setEventMetadata("VALID_TEAMID",",oparms:[{av:'A16TeamCountryId',fld:'TEAMCOUNTRYID',pic:'ZZZ9'},{av:'A17TeamCountryName',fld:'TEAMCOUNTRYNAME',pic:''},{av:'A30TeamContryFlag',fld:'TEAMCONTRYFLAG',pic:''},{av:'A40000TeamContryFlag_GXI',fld:'TEAMCONTRYFLAG_GXI',pic:''},{av:'A1DisciplineId',fld:'DISCIPLINEID',pic:'ZZZ9'},{av:'A2DisciplineName',fld:'DISCIPLINENAME',pic:''},{av:'Gx_mode',fld:'vMODE',pic:'@!'},{av:'Z14Teamid'},{av:'Z16TeamCountryId'},{av:'Z17TeamCountryName'},{av:'Z30TeamContryFlag'},{av:'Z40000TeamContryFlag_GXI'},{av:'Z1DisciplineId'},{av:'Z2DisciplineName'},{ctrl:'BTN_DELETE',prop:'Enabled'},{ctrl:'BTN_ENTER',prop:'Enabled'}]}");
         setEventMetadata("VALID_DISCIPLINEID","{handler:'Valid_Disciplineid',iparms:[{av:'A1DisciplineId',fld:'DISCIPLINEID',pic:'ZZZ9'},{av:'A2DisciplineName',fld:'DISCIPLINENAME',pic:''}]");
         setEventMetadata("VALID_DISCIPLINEID",",oparms:[{av:'A2DisciplineName',fld:'DISCIPLINENAME',pic:''}]}");
         setEventMetadata("VALID_ATHLETEID","{handler:'Valid_Athleteid',iparms:[{av:'A6AthleteId',fld:'ATHLETEID',pic:'ZZZ9'},{av:'A3CountryId',fld:'COUNTRYID',pic:'ZZZ9'},{av:'A29Athlete_Name',fld:'ATHLETE_NAME',pic:''},{av:'A4CountryName',fld:'COUNTRYNAME',pic:''}]");
         setEventMetadata("VALID_ATHLETEID",",oparms:[{av:'A29Athlete_Name',fld:'ATHLETE_NAME',pic:''},{av:'A3CountryId',fld:'COUNTRYID',pic:'ZZZ9'},{av:'A4CountryName',fld:'COUNTRYNAME',pic:''}]}");
         setEventMetadata("VALID_COUNTRYID","{handler:'Valid_Countryid',iparms:[]");
         setEventMetadata("VALID_COUNTRYID",",oparms:[]}");
         setEventMetadata("NULL","{handler:'Valid_Countryname',iparms:[]");
         setEventMetadata("NULL",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(24);
         pr_default.close(25);
         pr_default.close(5);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         Gx_mode = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A17TeamCountryName = "";
         A30TeamContryFlag = "";
         A40000TeamContryFlag_GXI = "";
         sImgUrl = "";
         A2DisciplineName = "";
         lblTitleathlete_Jsonclick = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gridteam_athleteContainer = new GXWebGrid( context);
         Gridteam_athleteColumn = new GXWebColumn();
         A29Athlete_Name = "";
         A4CountryName = "";
         sMode8 = "";
         sStyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         Z40000TeamContryFlag_GXI = "";
         Z2DisciplineName = "";
         T00059_A14Teamid = new short[1] ;
         T00059_A40000TeamContryFlag_GXI = new String[] {""} ;
         T00059_A2DisciplineName = new String[] {""} ;
         T00059_A1DisciplineId = new short[1] ;
         T00058_A2DisciplineName = new String[] {""} ;
         T000510_A2DisciplineName = new String[] {""} ;
         T000511_A14Teamid = new short[1] ;
         T00057_A14Teamid = new short[1] ;
         T00057_A40000TeamContryFlag_GXI = new String[] {""} ;
         T00057_A1DisciplineId = new short[1] ;
         sMode5 = "";
         T000512_A14Teamid = new short[1] ;
         T000513_A14Teamid = new short[1] ;
         T00056_A14Teamid = new short[1] ;
         T00056_A40000TeamContryFlag_GXI = new String[] {""} ;
         T00056_A1DisciplineId = new short[1] ;
         T000514_A14Teamid = new short[1] ;
         T000517_A2DisciplineName = new String[] {""} ;
         T000518_A22CompetitionId = new short[1] ;
         T000518_A14Teamid = new short[1] ;
         T000519_A14Teamid = new short[1] ;
         Z29Athlete_Name = "";
         Z4CountryName = "";
         T000520_A14Teamid = new short[1] ;
         T000520_A29Athlete_Name = new String[] {""} ;
         T000520_A4CountryName = new String[] {""} ;
         T000520_A6AthleteId = new short[1] ;
         T000520_A3CountryId = new short[1] ;
         T00054_A29Athlete_Name = new String[] {""} ;
         T00054_A3CountryId = new short[1] ;
         T00055_A4CountryName = new String[] {""} ;
         T000521_A29Athlete_Name = new String[] {""} ;
         T000521_A3CountryId = new short[1] ;
         T000522_A4CountryName = new String[] {""} ;
         T000523_A14Teamid = new short[1] ;
         T000523_A6AthleteId = new short[1] ;
         T00053_A14Teamid = new short[1] ;
         T00053_A6AthleteId = new short[1] ;
         T00052_A14Teamid = new short[1] ;
         T00052_A6AthleteId = new short[1] ;
         T000526_A29Athlete_Name = new String[] {""} ;
         T000526_A3CountryId = new short[1] ;
         T000527_A4CountryName = new String[] {""} ;
         T000528_A14Teamid = new short[1] ;
         T000528_A6AthleteId = new short[1] ;
         Gridteam_athleteRow = new GXWebRow();
         subGridteam_athlete_Linesclass = "";
         ROClassString = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXCCtlgxBlob = "";
         Z17TeamCountryName = "";
         Z30TeamContryFlag = "";
         ZZ17TeamCountryName = "";
         ZZ30TeamContryFlag = "";
         ZZ40000TeamContryFlag_GXI = "";
         ZZ2DisciplineName = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.team__default(),
            new Object[][] {
                new Object[] {
               T00052_A14Teamid, T00052_A6AthleteId
               }
               , new Object[] {
               T00053_A14Teamid, T00053_A6AthleteId
               }
               , new Object[] {
               T00054_A29Athlete_Name, T00054_A3CountryId
               }
               , new Object[] {
               T00055_A4CountryName
               }
               , new Object[] {
               T00056_A14Teamid, T00056_A40000TeamContryFlag_GXI, T00056_A1DisciplineId
               }
               , new Object[] {
               T00057_A14Teamid, T00057_A40000TeamContryFlag_GXI, T00057_A1DisciplineId
               }
               , new Object[] {
               T00058_A2DisciplineName
               }
               , new Object[] {
               T00059_A14Teamid, T00059_A40000TeamContryFlag_GXI, T00059_A2DisciplineName, T00059_A1DisciplineId
               }
               , new Object[] {
               T000510_A2DisciplineName
               }
               , new Object[] {
               T000511_A14Teamid
               }
               , new Object[] {
               T000512_A14Teamid
               }
               , new Object[] {
               T000513_A14Teamid
               }
               , new Object[] {
               T000514_A14Teamid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000517_A2DisciplineName
               }
               , new Object[] {
               T000518_A22CompetitionId, T000518_A14Teamid
               }
               , new Object[] {
               T000519_A14Teamid
               }
               , new Object[] {
               T000520_A14Teamid, T000520_A29Athlete_Name, T000520_A4CountryName, T000520_A6AthleteId, T000520_A3CountryId
               }
               , new Object[] {
               T000521_A29Athlete_Name, T000521_A3CountryId
               }
               , new Object[] {
               T000522_A4CountryName
               }
               , new Object[] {
               T000523_A14Teamid, T000523_A6AthleteId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000526_A29Athlete_Name, T000526_A3CountryId
               }
               , new Object[] {
               T000527_A4CountryName
               }
               , new Object[] {
               T000528_A14Teamid, T000528_A6AthleteId
               }
            }
         );
      }

      private short nIsMod_8 ;
      private short Z14Teamid ;
      private short Z1DisciplineId ;
      private short Z6AthleteId ;
      private short nRcdDeleted_8 ;
      private short nRcdExists_8 ;
      private short GxWebError ;
      private short A1DisciplineId ;
      private short A6AthleteId ;
      private short A3CountryId ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A14Teamid ;
      private short A16TeamCountryId ;
      private short subGridteam_athlete_Backcolorstyle ;
      private short subGridteam_athlete_Allowselection ;
      private short subGridteam_athlete_Allowhovering ;
      private short subGridteam_athlete_Allowcollapsing ;
      private short subGridteam_athlete_Collapsed ;
      private short nBlankRcdCount8 ;
      private short RcdFound8 ;
      private short nBlankRcdUsr8 ;
      private short GX_JID ;
      private short RcdFound5 ;
      private short nIsDirty_5 ;
      private short Gx_BScreen ;
      private short Z3CountryId ;
      private short nIsDirty_8 ;
      private short subGridteam_athlete_Backstyle ;
      private short gxajaxcallmode ;
      private short Z16TeamCountryId ;
      private short ZZ14Teamid ;
      private short ZZ16TeamCountryId ;
      private short ZZ1DisciplineId ;
      private int nRC_GXsfl_68 ;
      private int nGXsfl_68_idx=1 ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtTeamid_Enabled ;
      private int edtTeamCountryId_Enabled ;
      private int edtTeamCountryName_Enabled ;
      private int imgTeamContryFlag_Enabled ;
      private int edtDisciplineId_Enabled ;
      private int imgprompt_1_Visible ;
      private int edtDisciplineName_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int edtAthleteId_Enabled ;
      private int edtAthlete_Name_Enabled ;
      private int edtCountryId_Enabled ;
      private int edtCountryName_Enabled ;
      private int subGridteam_athlete_Selectedindex ;
      private int subGridteam_athlete_Selectioncolor ;
      private int subGridteam_athlete_Hoveringcolor ;
      private int fRowAdded ;
      private int subGridteam_athlete_Backcolor ;
      private int subGridteam_athlete_Allbackcolor ;
      private int imgprompt_6_Visible ;
      private int defedtAthleteId_Enabled ;
      private int idxLst ;
      private long GRIDTEAM_ATHLETE_nFirstRecordOnPage ;
      private String sPrefix ;
      private String sGXsfl_68_idx="0001" ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtTeamid_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtTeamid_Jsonclick ;
      private String edtTeamCountryId_Internalname ;
      private String edtTeamCountryId_Jsonclick ;
      private String edtTeamCountryName_Internalname ;
      private String A17TeamCountryName ;
      private String edtTeamCountryName_Jsonclick ;
      private String imgTeamContryFlag_Internalname ;
      private String sImgUrl ;
      private String edtDisciplineId_Internalname ;
      private String edtDisciplineId_Jsonclick ;
      private String imgprompt_1_Internalname ;
      private String imgprompt_1_Link ;
      private String edtDisciplineName_Internalname ;
      private String A2DisciplineName ;
      private String edtDisciplineName_Jsonclick ;
      private String divAthletetable_Internalname ;
      private String lblTitleathlete_Internalname ;
      private String lblTitleathlete_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String subGridteam_athlete_Header ;
      private String A29Athlete_Name ;
      private String A4CountryName ;
      private String sMode8 ;
      private String edtAthleteId_Internalname ;
      private String edtAthlete_Name_Internalname ;
      private String edtCountryId_Internalname ;
      private String edtCountryName_Internalname ;
      private String sStyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String Z2DisciplineName ;
      private String sMode5 ;
      private String Z29Athlete_Name ;
      private String Z4CountryName ;
      private String imgprompt_6_Internalname ;
      private String sGXsfl_68_fel_idx="0001" ;
      private String subGridteam_athlete_Class ;
      private String subGridteam_athlete_Linesclass ;
      private String imgprompt_6_Link ;
      private String ROClassString ;
      private String edtAthleteId_Jsonclick ;
      private String edtAthlete_Name_Jsonclick ;
      private String edtCountryId_Jsonclick ;
      private String edtCountryName_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private String subGridteam_athlete_Internalname ;
      private String Z17TeamCountryName ;
      private String ZZ17TeamCountryName ;
      private String ZZ2DisciplineName ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A30TeamContryFlag_IsBlob ;
      private bool bGXsfl_68_Refreshing=false ;
      private bool n40000TeamContryFlag_GXI ;
      private String A40000TeamContryFlag_GXI ;
      private String Z40000TeamContryFlag_GXI ;
      private String ZZ40000TeamContryFlag_GXI ;
      private String A30TeamContryFlag ;
      private String Z30TeamContryFlag ;
      private String ZZ30TeamContryFlag ;
      private GXWebGrid Gridteam_athleteContainer ;
      private GXWebRow Gridteam_athleteRow ;
      private GXWebColumn Gridteam_athleteColumn ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] T00059_A14Teamid ;
      private String[] T00059_A40000TeamContryFlag_GXI ;
      private String[] T00059_A2DisciplineName ;
      private short[] T00059_A1DisciplineId ;
      private String[] T00058_A2DisciplineName ;
      private String[] T000510_A2DisciplineName ;
      private short[] T000511_A14Teamid ;
      private short[] T00057_A14Teamid ;
      private String[] T00057_A40000TeamContryFlag_GXI ;
      private short[] T00057_A1DisciplineId ;
      private short[] T000512_A14Teamid ;
      private short[] T000513_A14Teamid ;
      private short[] T00056_A14Teamid ;
      private String[] T00056_A40000TeamContryFlag_GXI ;
      private short[] T00056_A1DisciplineId ;
      private short[] T000514_A14Teamid ;
      private String[] T000517_A2DisciplineName ;
      private short[] T000518_A22CompetitionId ;
      private short[] T000518_A14Teamid ;
      private short[] T000519_A14Teamid ;
      private short[] T000520_A14Teamid ;
      private String[] T000520_A29Athlete_Name ;
      private String[] T000520_A4CountryName ;
      private short[] T000520_A6AthleteId ;
      private short[] T000520_A3CountryId ;
      private String[] T00054_A29Athlete_Name ;
      private short[] T00054_A3CountryId ;
      private String[] T00055_A4CountryName ;
      private String[] T000521_A29Athlete_Name ;
      private short[] T000521_A3CountryId ;
      private String[] T000522_A4CountryName ;
      private short[] T000523_A14Teamid ;
      private short[] T000523_A6AthleteId ;
      private short[] T00053_A14Teamid ;
      private short[] T00053_A6AthleteId ;
      private short[] T00052_A14Teamid ;
      private short[] T00052_A6AthleteId ;
      private String[] T000526_A29Athlete_Name ;
      private short[] T000526_A3CountryId ;
      private String[] T000527_A4CountryName ;
      private short[] T000528_A14Teamid ;
      private short[] T000528_A6AthleteId ;
      private GXWebForm Form ;
   }

   public class team__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new UpdateCursor(def[22])
         ,new UpdateCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00059 ;
          prmT00059 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00058 ;
          prmT00058 = new Object[] {
          new Object[] {"@DisciplineId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000510 ;
          prmT000510 = new Object[] {
          new Object[] {"@DisciplineId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000511 ;
          prmT000511 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00057 ;
          prmT00057 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000512 ;
          prmT000512 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000513 ;
          prmT000513 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00056 ;
          prmT00056 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000514 ;
          prmT000514 = new Object[] {
          new Object[] {"@TeamContryFlag_GXI",SqlDbType.NVarChar,2048,0} ,
          new Object[] {"@DisciplineId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000515 ;
          prmT000515 = new Object[] {
          new Object[] {"@DisciplineId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000516 ;
          prmT000516 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000518 ;
          prmT000518 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000519 ;
          prmT000519 = new Object[] {
          } ;
          Object[] prmT000520 ;
          prmT000520 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00054 ;
          prmT00054 = new Object[] {
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00055 ;
          prmT00055 = new Object[] {
          new Object[] {"@CountryId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000521 ;
          prmT000521 = new Object[] {
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000522 ;
          prmT000522 = new Object[] {
          new Object[] {"@CountryId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000523 ;
          prmT000523 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00053 ;
          prmT00053 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00052 ;
          prmT00052 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000524 ;
          prmT000524 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000525 ;
          prmT000525 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000528 ;
          prmT000528 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000517 ;
          prmT000517 = new Object[] {
          new Object[] {"@DisciplineId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000526 ;
          prmT000526 = new Object[] {
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000527 ;
          prmT000527 = new Object[] {
          new Object[] {"@CountryId",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00052", "SELECT [Teamid], [AthleteId] FROM [TeamAthlete] WITH (UPDLOCK) WHERE [Teamid] = @Teamid AND [AthleteId] = @AthleteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00052,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00053", "SELECT [Teamid], [AthleteId] FROM [TeamAthlete] WHERE [Teamid] = @Teamid AND [AthleteId] = @AthleteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00053,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00054", "SELECT [Athlete_Name], [CountryId] FROM [Athlete] WHERE [AthleteId] = @AthleteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00054,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00055", "SELECT [CountryName] FROM [Country] WHERE [CountryId] = @CountryId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00055,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00056", "SELECT [Teamid], [TeamContryFlag_GXI], [DisciplineId] FROM [Team] WITH (UPDLOCK) WHERE [Teamid] = @Teamid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00056,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00057", "SELECT [Teamid], [TeamContryFlag_GXI], [DisciplineId] FROM [Team] WHERE [Teamid] = @Teamid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00057,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00058", "SELECT [DisciplineName] FROM [Discipline] WHERE [DisciplineId] = @DisciplineId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00058,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00059", "SELECT TM1.[Teamid], TM1.[TeamContryFlag_GXI], T2.[DisciplineName], TM1.[DisciplineId] FROM ([Team] TM1 INNER JOIN [Discipline] T2 ON T2.[DisciplineId] = TM1.[DisciplineId]) WHERE TM1.[Teamid] = @Teamid ORDER BY TM1.[Teamid]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00059,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000510", "SELECT [DisciplineName] FROM [Discipline] WHERE [DisciplineId] = @DisciplineId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000510,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000511", "SELECT [Teamid] FROM [Team] WHERE [Teamid] = @Teamid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000511,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000512", "SELECT TOP 1 [Teamid] FROM [Team] WHERE ( [Teamid] > @Teamid) ORDER BY [Teamid]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000512,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000513", "SELECT TOP 1 [Teamid] FROM [Team] WHERE ( [Teamid] < @Teamid) ORDER BY [Teamid] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000513,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000514", "INSERT INTO [Team]([TeamContryFlag_GXI], [DisciplineId]) VALUES(@TeamContryFlag_GXI, @DisciplineId); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000514)
             ,new CursorDef("T000515", "UPDATE [Team] SET [DisciplineId]=@DisciplineId  WHERE [Teamid] = @Teamid", GxErrorMask.GX_NOMASK,prmT000515)
             ,new CursorDef("T000516", "DELETE FROM [Team]  WHERE [Teamid] = @Teamid", GxErrorMask.GX_NOMASK,prmT000516)
             ,new CursorDef("T000517", "SELECT [DisciplineName] FROM [Discipline] WHERE [DisciplineId] = @DisciplineId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000517,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000518", "SELECT TOP 1 [CompetitionId], [Teamid] FROM [CompetitionTeam] WHERE [Teamid] = @Teamid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000518,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000519", "SELECT [Teamid] FROM [Team] ORDER BY [Teamid]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000519,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000520", "SELECT T1.[Teamid], T2.[Athlete_Name], T3.[CountryName], T1.[AthleteId], T2.[CountryId] FROM (([TeamAthlete] T1 INNER JOIN [Athlete] T2 ON T2.[AthleteId] = T1.[AthleteId]) INNER JOIN [Country] T3 ON T3.[CountryId] = T2.[CountryId]) WHERE T1.[Teamid] = @Teamid and T1.[AthleteId] = @AthleteId ORDER BY T1.[Teamid], T1.[AthleteId] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000520,11, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000521", "SELECT [Athlete_Name], [CountryId] FROM [Athlete] WHERE [AthleteId] = @AthleteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000521,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000522", "SELECT [CountryName] FROM [Country] WHERE [CountryId] = @CountryId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000522,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000523", "SELECT [Teamid], [AthleteId] FROM [TeamAthlete] WHERE [Teamid] = @Teamid AND [AthleteId] = @AthleteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000523,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000524", "INSERT INTO [TeamAthlete]([Teamid], [AthleteId]) VALUES(@Teamid, @AthleteId)", GxErrorMask.GX_NOMASK,prmT000524)
             ,new CursorDef("T000525", "DELETE FROM [TeamAthlete]  WHERE [Teamid] = @Teamid AND [AthleteId] = @AthleteId", GxErrorMask.GX_NOMASK,prmT000525)
             ,new CursorDef("T000526", "SELECT [Athlete_Name], [CountryId] FROM [Athlete] WHERE [AthleteId] = @AthleteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000526,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000527", "SELECT [CountryName] FROM [Country] WHERE [CountryId] = @CountryId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000527,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000528", "SELECT [Teamid], [AthleteId] FROM [TeamAthlete] WHERE [Teamid] = @Teamid ORDER BY [Teamid], [AthleteId] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000528,11, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getMultimediaUri(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                return;
             case 5 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getMultimediaUri(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                return;
             case 7 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getMultimediaUri(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 60) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                return;
             case 9 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 10 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 11 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 12 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                return;
             case 16 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 17 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 18 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 60) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 60) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                return;
             case 21 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 24 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                return;
             case 26 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 13 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 19 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 22 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 23 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 24 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
       }
    }

 }

}
