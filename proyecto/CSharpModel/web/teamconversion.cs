/*
               File: TeamConversion
        Description: Conversion for table Team
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/8/2022 12:55:22.36
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Reorg;
using System.Threading;
using GeneXus.Programs;
using System.Web.Services;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
namespace GeneXus.Programs {
   public class teamconversion : GXProcedure
   {
      public teamconversion( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public teamconversion( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         teamconversion objteamconversion;
         objteamconversion = new teamconversion();
         objteamconversion.context.SetSubmitInitialConfig(context);
         objteamconversion.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objteamconversion);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((teamconversion)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         cmdBuffer=" SET IDENTITY_INSERT [GXA0005] ON "
         ;
         RGZ = new GxCommand(dsDefault.Db, cmdBuffer, dsDefault,0,true,false,null);
         RGZ.ErrorMask = GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK;
         RGZ.ExecuteStmt() ;
         RGZ.Drop();
         /* Using cursor TEAMCONVER2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A15TeamAthleteId = TEAMCONVER2_A15TeamAthleteId[0];
            A1DisciplineId = TEAMCONVER2_A1DisciplineId[0];
            A14Teamid = TEAMCONVER2_A14Teamid[0];
            /*
               INSERT RECORD ON TABLE GXA0005

            */
            AV2Teamid = A14Teamid;
            AV3DisciplineId = A1DisciplineId;
            AV4TeamAthleteId = A15TeamAthleteId;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A11TeamAthleteName)) )
            {
               AV5TeamAthleteName = " ";
            }
            else
            {
               AV5TeamAthleteName = A11TeamAthleteName;
            }
            /* Using cursor TEAMCONVER3 */
            pr_default.execute(1, new Object[] {AV2Teamid, AV3DisciplineId, AV4TeamAthleteId, AV5TeamAthleteName});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("GXA0005") ;
            if ( (pr_default.getStatus(1) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(GXResourceManager.GetMessage("GXM_noupdate"));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            pr_default.readNext(0);
         }
         pr_default.close(0);
         cmdBuffer=" SET IDENTITY_INSERT [GXA0005] OFF "
         ;
         RGZ = new GxCommand(dsDefault.Db, cmdBuffer, dsDefault,0,true,false,null);
         RGZ.ErrorMask = GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK;
         RGZ.ExecuteStmt() ;
         RGZ.Drop();
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         cmdBuffer = "";
         scmdbuf = "";
         TEAMCONVER2_A15TeamAthleteId = new short[1] ;
         TEAMCONVER2_A1DisciplineId = new short[1] ;
         TEAMCONVER2_A14Teamid = new short[1] ;
         A11TeamAthleteName = "";
         AV5TeamAthleteName = "";
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.teamconversion__default(),
            new Object[][] {
                new Object[] {
               TEAMCONVER2_A15TeamAthleteId, TEAMCONVER2_A1DisciplineId, TEAMCONVER2_A14Teamid
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A15TeamAthleteId ;
      private short A1DisciplineId ;
      private short A14Teamid ;
      private short AV2Teamid ;
      private short AV3DisciplineId ;
      private short AV4TeamAthleteId ;
      private int GIGXA0005 ;
      private String cmdBuffer ;
      private String scmdbuf ;
      private String A11TeamAthleteName ;
      private String AV5TeamAthleteName ;
      private String Gx_emsg ;
      private IGxDataStore dsDefault ;
      private GxCommand RGZ ;
      private IDataStoreProvider pr_default ;
      private short[] TEAMCONVER2_A15TeamAthleteId ;
      private short[] TEAMCONVER2_A1DisciplineId ;
      private short[] TEAMCONVER2_A14Teamid ;
   }

   public class teamconversion__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmTEAMCONVER2 ;
          prmTEAMCONVER2 = new Object[] {
          } ;
          Object[] prmTEAMCONVER3 ;
          prmTEAMCONVER3 = new Object[] {
          new Object[] {"@AV2Teamid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV3DisciplineId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV4TeamAthleteId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV5TeamAthleteName",SqlDbType.NChar,60,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("TEAMCONVER2", "SELECT [TeamAthleteId], [DisciplineId], [Teamid] FROM [Team] ORDER BY [Teamid] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmTEAMCONVER2,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("TEAMCONVER3", "INSERT INTO [GXA0005]([Teamid], [DisciplineId], [TeamAthleteId], [TeamAthleteName]) VALUES(@AV2Teamid, @AV3DisciplineId, @AV4TeamAthleteId, @AV5TeamAthleteName)", GxErrorMask.GX_NOMASK,prmTEAMCONVER3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
       }
    }

 }

}
